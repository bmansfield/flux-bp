# Flux CI CD Boilerplate

This is my flux ci/cd boilerplate repository. A repo meant for a good starting
place for all of your future flux projects.

Loosely follows their [installation
guide](https://fluxcd.io/flux/installation/#gitlab-and-gitlab-enterprise).

After you create your token and `export GITLAB_TOKEN=<your-token>` it.

```bash
make init REPO=<your-repo>
```


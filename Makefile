REPO ?= flux-bp
USERNAME ?= bmansfield
BRANCH ?= main
CLUSTER ?= my-cluster

all: install

default: install

init setup:
	flux bootstrap gitlab \
		--owner=$(USERNAME) \
		--repository=$(REPO) \
		--branch=$(BRANCH) \
		--path=clusters/$(CLUSTER) \
		--token-auth \
		--personal

install deploy:
	install-cmd-goes-here

ls list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

.PHONY: init setup deploy install ls list
